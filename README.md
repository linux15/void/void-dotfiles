## Void Dotfiles

Dotfiles for my Void Linux setup with BSPWM. You should have the following installed: 

### Applications
- BSPWM (Window Manager)
- Polybar (Toolbar)
- sxhkd (Keybindings)
- picom (Compositor)
- uxvrt (Terminal emulator)
- Betterlockscreen (minimal screenlock)
- Bottom (system monitor)
- Ranger (file manager)
- Spotify
- Spicetify (CLI tools for Spotify theming)
- Cava (Audio visualizer)
- Dunst (notification manager)

### Fonts
- Ioveska
- Material Design

### Notes
- BSPWM is set up for my personal setup and application usage. Modify at your leisure.
- Polybar includes scripts to view currently playing Spotify song as well as a script to check if my Mullvad Wireguard connection is up or not. Modify at your leisure.
